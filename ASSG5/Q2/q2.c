#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>

typedef struct Graph
{
    int V;
    int E;
    int **graph;
} Graph;

Graph *createGraph(int vertices, int edges)
{
    Graph *G = (Graph *)malloc(sizeof(Graph));
    G->V = vertices;
    G->E = edges;
    G->graph = malloc(G->V * sizeof(int *));
    for (int i = 0; i < G->V; ++i)
        G->graph[i] = malloc(G->V * sizeof(int));

    for (int i = 0; i < G->V; ++i)
        for (int j = 0; j < G->V; ++j)
            G->graph[i][j] = 0;
    return G;
}

int minDist(int distance[], bool sptSet[], int v)
{
    int min_dist = INT_MAX, min_index;
    for (int i = 0; i < v; ++i)
    {
        if (sptSet[i] == false && distance[i] <= min_dist)
        {
            min_dist = distance[i];
            min_index = i;
        }
    }
    return min_index;
}

void printPath(int parent[], int j)
{
    if (parent[j] == -1)
    {
        printf("%d", j + 1);
        return;
    }

    printPath(parent, parent[j]);
    printf("->%d", j + 1);
}

void dijikstra(Graph *G, int src)
{
    int v = G->V;
    int **graph = G->graph;
    int distnace[v], parent[v], steps = 0;
    bool sptSet[v];
    for (int i = 0; i < v; ++i)
    {
        parent[i] = -1;
        distnace[i] = INT_MAX;
        sptSet[i] = false;
    }
    distnace[src] = 0;
    for (int count = 0; count < v - 1; ++count)
    {
        int i = minDist(distnace, sptSet, v);
        sptSet[i] = true;
        for (int j = 0; j < v; ++j)
        {
            if (!sptSet[j] && graph[i][j])
            {
                if (distnace[i] != INT_MAX && distnace[i] + graph[i][j] < distnace[j])
                {
                    parent[j] = i;
                    distnace[j] = distnace[i] + graph[i][j];
                }
            }
        }
    }

    // print the details
    printf("\n");
    printf("LSR at node : %d", src + 1);
    printf("\ndest\tdist\tpath");
    for (int i = 0; i < v; ++i)
    {
        printf("\n%d\t", i + 1);
        printf("%d\t", distnace[i]);
        if (i == src)
            printf("%d->%d", i + 1, i + 1);
        else
            printPath(parent, i);
    }
    printf("\n");
}

int main()
{
    int V, E;
    scanf("%d %d", &V, &E);

    // create a graph
    Graph *G = createGraph(V, E);

    // add edges
    int src, dest, weight;
    for (int i = 0; i < G->E; ++i)
    {
        scanf("%d %d %d", &src, &dest, &weight);
        G->graph[src][dest] = weight;
        G->graph[dest][src] = weight;
    }
    for (int i = 0; i < G->V; ++i)
        dijikstra(G, i);
    printf("\n");
    return (0);
}