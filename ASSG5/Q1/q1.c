#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <stdbool.h>

typedef struct edge
{
    int src;
    int dest;
    int weight;
} edge;

typedef struct Graph
{
    int V;
    int E;
    edge *edge;
} Graph;

Graph *createGraph(int vertices, int edges)
{
    Graph *G = (Graph *)malloc(sizeof(Graph));
    G->V = vertices;
    G->E = edges;
    G->edge = (edge *)malloc(edges * sizeof(edge));
    return G;
}

void nextHop(int parent[], int j, int *prevNode)
{
    if (parent[j] == -1)
        return;

    *prevNode = j + 1;
    nextHop(parent, parent[j], prevNode);
}

void bellmanFord(Graph *G, int src)
{
    int vertices = G->V, edges = G->E;
    int distance[vertices], parent[vertices];
    for (int i = 0; i < vertices; ++i)
    {
        parent[i] = -1;
        distance[i] = INT_MAX;
    }
    distance[src] = 0;
    for (int i = 0; i < vertices - 1; ++i)
    {
        for (int j = 0; j < edges; ++j)
        {
            edge *e = &G->edge[j];
            int u = e->src;
            int v = e->dest;
            int w = e->weight;

            if (distance[u] != INT_MAX && distance[v] > distance[u] + w)
            {
                parent[v] = u;
                distance[v] = distance[u] + w;
            }

            if (distance[v] != INT_MAX && distance[u] > distance[v] + w)
            {
                parent[u] = v;
                distance[u] = distance[v] + w;
            }
        }
    }
    for (int j = 0; j < edges; ++j)
    {
        edge e = G->edge[j];
        int u = e.src;
        int v = e.dest;
        int w = e.weight;

        if (distance[u] != INT_MAX && distance[v] > distance[u] + w)
            return;
        if (distance[v] != INT_MAX && distance[u] > distance[v] + w)
            return;
    }

    // print the details
    printf("\n");
    printf("DVR at node : %d", src + 1);

    printf("\ndest\thop\tdist");
    for (int i = 0; i < vertices; ++i)
    {
        printf("\n%d\t", i + 1);
        int prevNode = src + 1;
        nextHop(parent, i, &prevNode);
        printf("%d\t", prevNode);
        printf("%d\t", distance[i]);
    }
    printf("\n");
    return;
}

int main()
{
    int V, E;
    scanf("%d %d", &V, &E);

    // create a graph
    Graph *G = createGraph(V, E);

    // add edges
    int src, dest, weight;
    for (int i = 0; i < G->E; ++i)
    {
        scanf("%d %d %d", &src, &dest, &weight);
        edge *e = &G->edge[i];
        e->src = src - 1;
        e->dest = dest - 1;
        e->weight = weight;
    }
    // bellmanFord(G, 0);
    for (int i = 0; i < G->V; ++i)
    {
        bellmanFord(G, i);
    }
    printf("\n");
    return (0);
}