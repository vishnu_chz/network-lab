#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <ctype.h>
// #include "eval.h"

#define PORT 8080
#define BUFFER_SIZE 1024
#define LOCAL_HOST "127.0.0.1"

typedef struct client
{
    struct sockaddr_in clientaddr;
    int idx;
} client;

int evaluate(char buffer[])
{
    int flag = 0;
    float n1 = 0;
    float n2 = 0;
    int op;

    for (int i = 0; buffer[i] != '\0'; i++)
    {
        if (!isdigit(buffer[i]))
        {
            flag = 1;
            op = i;
        }
        else
        {
            if (flag == 0)
            {
                n1 = (n1 * 10) + (buffer[i] - '0');
            }
            else if (flag != 0)
            {
                n2 = (n2 * 10) + (buffer[i] - '0');
            }
        }
    }
    int result = 0;
    if (buffer[op] == '+')
        result = n1 + n2;
    else if (buffer[op] == '-')
        result = n1 - n2;
    else if (buffer[op] == '/')
        result = n1 / n2;
    else if (buffer[op] == '*')
        result = n1 * n2;
    return result;
}

int main()
{
    int sockfd, connsockfd, client_count = 1;
    struct sockaddr_in servaddr, clientaddr;
    char buffer[BUFFER_SIZE];
    socklen_t addr_size;
    pid_t childpid;
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
        error("SOCKET CREATION FAILED");
    else
        printf("SOCKET CREATED SUCCESFULLY...\n");

    memset(&servaddr, '\0', sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr = inet_addr(LOCAL_HOST);

    if (bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
        error("SOCKET BINDING FAILED\n");
    else
        printf("SOCKET BINDING SUCCESFULL...\n");
    if (listen(sockfd, 10) == -1)
        error("SERVER LISTENING FAILED");
    else
        printf("LISTENING FOR CLIENT...\n");

    while (1)
    {
        client c;
        connsockfd = accept(sockfd, (struct sockaddr *)&clientaddr, &addr_size);
        if (connsockfd == -1)
            error("CONNECTION TO CLIENT FAILED");
        else
        {

            c.clientaddr = clientaddr;
            c.idx = client_count;
            client_count += 1;
            printf("-- CONNECTED WITH CLIENT NUMBER %d --\n", c.idx);
            // printf("CONNECTION ACCEPTED FROM %s:%d\n", inet_ntoa(c.clientaddr.sin_addr), ntohs(c.clientaddr.sin_port));
        }
        if ((childpid = fork()) == 0)
        {
            bzero(buffer, BUFFER_SIZE);
            close(sockfd);
            while (1)
            {
                recv(connsockfd, buffer, BUFFER_SIZE, 0);
                if (strcmp(buffer, "exit") == 0)
                {
                    printf("-- DISCONNECTED FROM CLIENT NUMBER %d --\n", c.idx);
                    // printf("DISCONNECTED FROM %s:%d\n", inet_ntoa(clientaddr.sin_addr), ntohs(clientaddr.sin_port));
                    break;
                }
                else
                {
                    printf("Client %d : %s\n", c.idx, buffer);
                    snprintf(buffer, BUFFER_SIZE, "%d", evaluate(buffer));
                    send(connsockfd, buffer, strlen(buffer), 0);
                    printf("Server : %s\n", buffer);
                    bzero(buffer, BUFFER_SIZE);
                }
            }
        }
    }
    close(sockfd);
    close(connsockfd);

    return (0);
}