#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <signal.h>

#define PORT 8080
#define BUFFER_SIZE 1024
#define LOCAL_HOST "127.0.0.1"

/* A C program that does not terminate when Ctrl+C is pressed */
#include <stdio.h>
#include <signal.h>

int sockfd;
void trap(int sig_num)
{
    signal(SIGINT, &trap);
    char buffer[BUFFER_SIZE] = "exit";
    send(sockfd, buffer, strlen(buffer), 0);
    close(sockfd);
    printf("\nDISCONNECTED FROM THE SERVER...\n");
    exit(0);
}

void error(char *msg)
{
    perror(msg);
    printf("\n");
    exit(0);
}

int main()
{
    struct sockaddr_in servaddr;
    char buffer[BUFFER_SIZE];
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
        error("SOCKET CREATION FAILED...");
    else
        printf("SOCKET CREATED SUCCESFULLY...\n");
    memset(&servaddr, '\0', sizeof(servaddr));
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr = inet_addr(LOCAL_HOST);

    if (connect(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr)) == -1)
        error("CONNECTION TO SOCKET FAILED...");
    else
        printf("CONNECTION TO SOCKET SUCCESFUL...\n");
    signal(SIGINT, &trap);
    while (1)
    {
        printf("CLIENT : ");
        fgets(buffer, BUFFER_SIZE, stdin);
        buffer[strlen(buffer) - 1] = '\0';
        send(sockfd, buffer, strlen(buffer), 0);
        if (strcmp(buffer, "exit") == 0)
            break;
        if (recv(sockfd, buffer, BUFFER_SIZE, 0) == -1)
            printf("ERROR READING DATA...\n");
        else
            printf("SERVER : %s\n", buffer);
    }
    close(sockfd);
    printf("DISCONNECTED FROM THE SERVER...\n");
    return (0);
}