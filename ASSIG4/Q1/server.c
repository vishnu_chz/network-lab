#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <pthread.h>
#include <arpa/inet.h>

#define PORT 8080
#define BUFFER_SIZE 1024

pthread_mutex_t lock;
pthread_t close_t;

typedef struct client
{

    int index, sockfd, len, alive;
    struct sockaddr_in client_addr;
    char client_name[BUFFER_SIZE];
} client;

client clients[BUFFER_SIZE];
pthread_t thread[BUFFER_SIZE];
int client_count = 0;

// for ERRORs
void error(const char *buffer)
{
    perror(buffer);
    printf("\n");
    exit(1);
}

// close the server
void *endserver(void *sockfd)
{
    char buffer[BUFFER_SIZE];
    while (1)
    {
        scanf("%s", buffer);
        if (strcmp(buffer, "tata") == 0)
            break;
    }
    int newsockfd = *((int *)sockfd);
    close(newsockfd);
    printf("SERVER IS DISCONNECTED\n");
    exit(1);
}

// THE CHAT SYSTEM
void *chat(void *args)
{
    // Implementing details about client
    client *target_client = (client *)args;
    int index = target_client->index;
    int client_socket = target_client->sockfd;
    target_client->alive = 1;
    char client_name[128];

    recv(client_socket, client_name, 128, 0);
    strcpy(target_client->client_name, client_name);

    // Broadcast that Client joined the Chat
    char joinmsg[2048];
    snprintf(joinmsg, 2048, "%s has entered the room.\n", target_client->client_name);
    printf("%s has entered the room at socket [ %d ]\n", target_client->client_name, target_client->sockfd);
    for (int i = 0; i < client_count; i++)
    {
        if (i != index && clients[i].alive != 0)
            send(clients[i].sockfd, joinmsg, 2048, 0);
    }

    // ACTUAL CHAT SYSTEM
    while (1)
    {

        char buffer[BUFFER_SIZE];
        char msg[2048];
        int read = recv(client_socket, buffer, BUFFER_SIZE, 0);
        buffer[read] = '\0';
        char output[BUFFER_SIZE];

        // if (strcmp(buffer, "DISP") == 0)
        // {
        //     pthread_mutex_lock(&lock); // lock the current thread
        //     int l = 0;

        //     for (int i = 0; i < client_count; i++)
        //     {
        //         if (clients[i].alive == 0)
        //             continue;

        //         else if (i != index)
        //             l += snprintf(output + l, BUFFER_SIZE, "%s is on socket %d.\n", clients[i].client_name, clients[i].sockfd);

        //         else
        //             l += snprintf(output + l, BUFFER_SIZE, "(You)%s are on socket %d.\n", client_name, client_socket);
        //     }

        //     send(client_socket, output, BUFFER_SIZE, 0);
        //     pthread_mutex_unlock(&lock); // unlock the thread
        //     continue;
        // }

        if (strcmp(buffer, "bye") == 0 || read == 0) // Broadcast that client left
        {
            pthread_mutex_lock(&lock); // lock the thread

            clients[index].alive = 0;
            printf("%s has left the room .\n", target_client->client_name);

            snprintf(buffer, BUFFER_SIZE, "%s has left the room.\n", target_client->client_name);
            for (int i = 0; i < client_count; i++)
            {
                if (i != index && clients[i].alive != 0)
                    send(clients[i].sockfd, buffer, BUFFER_SIZE, 0);
            }
            pthread_mutex_unlock(&lock); // unlock the thread

            bzero(buffer, BUFFER_SIZE);
            int *temp;

            pthread_exit(temp); // terminate the corresponding thread
        }
        else
        {
            printf("%s", buffer);

            pthread_mutex_lock(&lock);  // lock the current thread
            bzero(buffer, BUFFER_SIZE); // clear the buffer

            read = recv(client_socket, buffer, BUFFER_SIZE, 0);
            buffer[read] = '\0';

            snprintf(msg, 2048, "%s: %s", target_client->client_name, buffer);

            for (int i = 0; i < client_count; i++)
            {
                if (i != index && clients[i].alive != 0)
                    send(clients[i].sockfd, msg, 2048, 0);
            }

            pthread_mutex_unlock(&lock); // unlock the thread
        }
    }

    fprintf(stderr, "Here somehow");
    return NULL;
}

// Driver function
int main()
{
    int sockfd;
    struct sockaddr_in servaddr; // Internet address

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
        error("socket creation failed... ");
    else
        printf("socket created succesfully... \n");

    bzero(&servaddr, sizeof(servaddr));
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (struct sockaddr *)&servaddr, sizeof(servaddr))) != 0)
        error("socket binding failed...");
    else
        printf("socket is binded , server starterd ...\n");

    // Now server is ready to listen and verify
    if ((listen(sockfd, BUFFER_SIZE)) != 0)
        error("server failed to listen\n");
    else
        printf("server is listening\n");

    // Checking if the server is to be closed
    pthread_create(&close_t, NULL, endserver, (void *)&sockfd);

    // Accept the data packet from client and verify
    while (1)
    {
        // accept
        clients[client_count].sockfd = accept(sockfd, (struct sockaddr *)&clients[client_count].client_addr, &clients[client_count].len);
        clients[client_count].index = client_count;

        // create a thread for a connection
        pthread_create(&thread[client_count], NULL, chat, (void *)&clients[client_count]);
        client_count = client_count + 1;
    }

    return 0;
}
