#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>

#define PORT 4053
#define BUFFER_SIZE 1028
#define DISK "./"
#define LOCAL_HOST "127.0.0.1"

void error(char *msg)
{
    perror(msg);
    exit(0);
}

int fileExist(char *fname)
{
    int found = 0;
    DIR *dir_ptr;
    struct dirent *dir;
    dir_ptr = opendir(DISK);
    while ((dir = readdir(dir_ptr)) != NULL)
    {
        if (strcmp(dir->d_name, fname) == 0)
        {
            found = 1;
            break;
        }
    }
    closedir(dir_ptr);
    return found;
}

int getFileSize(char filename[])
{
    struct stat st;
    stat(filename, &st);
    int size = st.st_size;
    return size;
}

int sendFile(int sockfd, char file_name[])
{
    FILE *fp = fopen(file_name, "r");
    if (fp == NULL)
    {
        printf("File open error");
        return 1;
    }

    while (1)
    {
        int BUFF_SZ = 500;
        char sendBuff[BUFF_SZ];
        bzero(sendBuff, BUFF_SZ);
        int nread = fread(sendBuff, 1, BUFF_SZ, fp);

        if (nread > 0)
        {
            write(sockfd, sendBuff, nread);
        }
        if (nread < BUFF_SZ)
        {
            if (feof(fp))
            {
                printf("End of file\n");
                printf("File Transfer Completed\n");
            }
            if (ferror(fp))
                printf("Error reading\n");
            break;
        }
    }

    printf("This File Is Sent\n");
    return 0;
}

int receiveFile(int sockfd, char file_name[], int size)
{
    char recvBuff[500];
    memset(recvBuff, '0', sizeof(recvBuff)); // put all 0s in recvbuff
    FILE *fp;
    printf("Receiving file...\n");
    fp = fopen(file_name, "w");
    if (NULL == fp)
    {
        printf("Error opening file");
        return 1;
    }
    int total = 0;
    int bytesReceived;
    while ((bytesReceived = read(sockfd, recvBuff, 500)) > 0)
    {
        total += bytesReceived;
        fflush(stdout);
        fwrite(recvBuff, 1, bytesReceived, fp);
        memset(recvBuff, '0', sizeof(recvBuff));
        if (total >= size)
        {
            printf("File Receiving....\nCompleted\n");
            return 0;
        }
    }

    if (bytesReceived < 0)
    {
        printf("\n Read Error \n");
        return 0;
    }

    printf("\nFile Receiving....Completed\n");
    return 0;
}

int main()
{
    struct sockaddr_in server_addr, client_addr;
    int sockfd, start = -1;
    char buffer[BUFFER_SIZE];
    char username[30], password[30];
    bzero(buffer, BUFFER_SIZE);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("socket creation failed");
    else
        printf("socket creation succesfull\n");

    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(PORT);
    if (inet_pton(AF_INET, LOCAL_HOST, &server_addr.sin_addr) <= 0)
        error("\nError: Invalid address. Address not supported. ");

    if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) < 0)
        error("\nError: Failed to connect to the remote host. ");

    while (1)
    {
        bzero(buffer, BUFFER_SIZE);
        fgets(buffer, BUFFER_SIZE, stdin);
        buffer[strcspn(buffer, "\n")] = 0;
        char command_line[BUFFER_SIZE];
        strcpy(command_line, buffer);
        char *command = strtok(command_line, " ");
        if (strcmp("START", command) == 0)
        {
            printf("please enter username and password as \n USERN <username> \n PASS <password> \n");
            strcpy(username, "");
            strcpy(password, "");
        }
        else if (strcmp("QUIT", command) == 0)
        {
            send(sockfd, buffer, BUFFER_SIZE, 0);
            break;
        }
        else if (strcmp("USERN", command) == 0)
        {
            command = strtok(NULL, " ");
            strcpy(username, command);
            send(sockfd, buffer, BUFFER_SIZE, 0);
            bzero(buffer, BUFFER_SIZE);
            recv(sockfd, buffer, BUFFER_SIZE, 0);
            int stat_code = atoi(buffer);
            if (stat_code == 300)
                printf("username is valid please enter password\n");
            else if (stat_code == 301)
            {
                bzero(buffer, BUFFER_SIZE);
                strcpy(username, "");
                printf("incorrect username please re-enter username\n");
            }
        }
        else if (strcmp(command, "PASSWD") == 0)
        {
            if (strcmp(username, "") == 0)
            {
                printf("please enter the username\n");
                break;
            }
            command = strtok(NULL, " ");
            strcpy(password, command);
            send(sockfd, buffer, BUFFER_SIZE, 0);
            bzero(buffer, BUFFER_SIZE);
            recv(sockfd, buffer, BUFFER_SIZE, 0);
            int stat_code = atoi(buffer);
            if (stat_code == 305)
            {
                printf("\n\nAccess Granted\n");
                printf("user authenticated with password\n\n");
                printf("1) CreateFile <filename> : creates a file with given name in server\n");
                printf("2) ListDir : displays all the files in the server\n");
                printf("3) StoreFile <filename> : stores the give local file in the server\n");
                printf("4) GetFile <filename> : retrieve specific files from the server\n");
                printf("5) QUIT : quits the ftp server\n");
                printf("6) START : relogin ftp server\n\n");
            }
            else if (stat_code == 310)
            {
                bzero(buffer, BUFFER_SIZE);
                strcpy(password, "");
                printf("incorrect username please re-enter password\n");
            }
        }
        else if (strcmp("CreateFile", command) == 0)
        {
            char *file_name;
            command = strtok(NULL, " ");
            file_name = command;
            if (file_name == NULL)
            {
                printf("please enter the filename and try again\n");
                continue;
            }
            send(sockfd, buffer, BUFFER_SIZE, 0);
            printf("client requested to CREATE %s\n", file_name);
        }
        else if (strcmp("ListDir", command) == 0)
        {
            send(sockfd, buffer, BUFFER_SIZE, 0);
            recv(sockfd, buffer, BUFFER_SIZE, 0);
            printf("LIST OF FILES\n%s\n", buffer);
        }
        else if (strcmp("StoreFile", command) == 0)
        {
            char *file_name;
            command = strtok(NULL, " ");
            file_name = command;
            if (file_name == NULL)
            {
                printf("please enter the filename and try again\n");
                continue;
            }
            if (!fileExist(file_name))
            {
                printf("ERROR : THe file %s does not exists in your DISK\n", file_name);
                continue;
            }
            int size = getFileSize(file_name);
            snprintf(buffer, BUFFER_SIZE, "StoreFile %s %d", file_name, size);
            printf("CLIENT : transferring %s to server.\n", buffer);
            send(sockfd, buffer, BUFFER_SIZE, 0);
            printf("\nInitiating the Sending Process...\n");
            sendFile(sockfd, file_name);
        }
        else if (strcmp("GetFile", command) == 0)
        {
            char *file_name;
            command = strtok(NULL, " ");
            file_name = command;
            if (file_name == NULL)
            {
                printf("please enter the filename and try again\n");
                continue;
            }
            printf("client requested to GET %s\n", file_name);
            send(sockfd, buffer, BUFFER_SIZE, 0);
            recv(sockfd, buffer, BUFFER_SIZE, 0);
            printf("%s\n", buffer);
            char response[BUFFER_SIZE];
            strcpy(response, buffer);
            char *status = strtok(response, " ");
            int stat_code = atoi(status);
            printf("%d\n", stat_code);
            if (stat_code == 400)
                printf("file %s not present in the server\n", file_name);
            else if (stat_code == 200)
            {
                status = strtok(NULL, " ");
                int size = atoi(status);
                printf("fetching file %s from server\n", file_name);
                receiveFile(sockfd, file_name, size);
            }
            continue;
        }
        else
        {
            printf("Invalid Command\n");
            continue;
        }
    }
    close(sockfd);
    return (0);
}