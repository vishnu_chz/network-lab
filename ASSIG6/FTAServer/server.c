#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <errno.h>
#include <dirent.h>

#define PORT 4053
#define BUFFER_SIZE 1028
#define DISK "./"
#define USER "root"
#define PASS "root@1234"

void error(char *msg)
{
    perror(msg);
    exit(0);
}

// 200 OK Connection is set up
// 300 Correct Username Need password
// 301 Incorrect Username
// 305 User Authenticated with password
// 310 Incorrect password
// 505 Command not supported

int fileExist(char *fname)
{
    int found = 0;
    DIR *dir_ptr;
    struct dirent *dir;
    dir_ptr = opendir(DISK);
    while ((dir = readdir(dir_ptr)) != NULL)
    {
        if (strcmp(dir->d_name, fname) == 0)
        {
            found = 1;
            break;
        }
    }
    closedir(dir_ptr);
    return found;
}

int getFileSize(char filename[])
{
    struct stat st;
    stat(filename, &st);
    int size = st.st_size;
    return size;
}

int sendFile(int sockfd, char file_name[])
{
    FILE *fp = fopen(file_name, "r");
    if (fp == NULL)
    {
        printf("File open error");
        return 1;
    }

    while (1)
    {
        int BUFF_SZ = 500;
        char sendBuff[BUFF_SZ];
        bzero(sendBuff, BUFF_SZ);
        int nread = fread(sendBuff, 1, BUFF_SZ, fp);

        if (nread > 0)
        {
            write(sockfd, sendBuff, nread);
        }
        if (nread < BUFF_SZ)
        {
            if (feof(fp))
            {
                printf("End of file\n");
                printf("File Transfer Completed\n");
            }
            if (ferror(fp))
                printf("Error reading\n");
            break;
        }
    }

    printf("This File Is Sent\n");
    return 0;
}

int receiveFile(int sockfd, char file_name[], int size)
{
    char recvBuff[500];
    memset(recvBuff, '0', sizeof(recvBuff)); // put all 0s in recvbuff
    FILE *fp;
    printf("Receiving file...\n");
    fp = fopen(file_name, "w");
    if (NULL == fp)
    {
        printf("Error opening file");
        return 1;
    }
    int total = 0;
    int bytesReceived;
    while ((bytesReceived = read(sockfd, recvBuff, 500)) > 0)
    {
        total += bytesReceived;
        fflush(stdout);
        fwrite(recvBuff, 1, bytesReceived, fp);
        memset(recvBuff, '0', sizeof(recvBuff));
        if (total >= size)
        {
            printf("File Receiving....\nCompleted\n");
            return 0;
        }
    }

    if (bytesReceived < 0)
    {
        printf("\n Read Error \n");
        return 0;
    }

    printf("\nFile Receiving....Completed\n");
    return 0;
}

void listDir(char files[])
{
    DIR *dir_ptr;
    struct dirent *dir;
    dir_ptr = opendir(DISK);
    while ((dir = readdir(dir_ptr)) != NULL)
    {
        if (strcmp(dir->d_name, ".") != 0 && strcmp(dir->d_name, "..") != 0)
        {
            char file[10];
            strcat(files, dir->d_name);
            strcat(files, "\n");
        }
    }
    closedir(dir_ptr);
    return;
}

void createFile(char file_name[])
{
    FILE *file;
    file = fopen(file_name, "w");
    fclose(file);
    printf("creating file %s\n", file_name);
    return;
}

int main()
{
    int sockfd;
    struct sockaddr_in client_addr;
    char buffer[BUFFER_SIZE];
    bzero(buffer, BUFFER_SIZE);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0)
        error("socket creation failed");
    else
        printf("socket creation succesfull\n");

    client_addr.sin_family = AF_INET;
    client_addr.sin_addr.s_addr = INADDR_ANY;
    client_addr.sin_port = htons(PORT);

    if (bind(sockfd, (struct sockaddr *)&client_addr, (socklen_t)sizeof(client_addr)) < 0)
        error("server binding failed");
    else
        printf("socket binding succesfull\n");

    if (listen(sockfd, 10) < 0)
        error("server failed to listen");
    else
        printf("server is listening at PORT : %d\n", PORT);

    while (1)
    {
        int accept_sockfd;
        int auth = 0;
        int addr_len = sizeof(client_addr);
        if ((accept_sockfd = accept(sockfd, (struct sockaddr *)&client_addr, (socklen_t *)&addr_len)) < 0)
            error("accept");
        printf("Connection received\n");
        char username[30], password[30];
        strcpy(username, "");
        strcpy(password, "");
        while (1)
        {
            recv(accept_sockfd, buffer, BUFFER_SIZE, 0);
            char command_line[BUFFER_SIZE];
            strcpy(command_line, buffer);
            char *command = strtok(command_line, " ");
            if (strcmp("QUIT", command) == 0)
                break;
            else if (strcmp(command, "START") == 0)
            {
                strcpy(username, "");
                strcpy(password, "");
                bzero(buffer, BUFFER_SIZE);
                auth = 0;
            }
            else if (strcmp(command, "USERN") == 0)
            {
                command = strtok(NULL, " ");
                strcpy(username, command);
                if (strcmp(username, USER) == 0)
                {
                    printf("300 Correct Username Need password\n");
                    strcpy(buffer, "300");
                    send(accept_sockfd, buffer, BUFFER_SIZE, 0);
                    bzero(buffer, BUFFER_SIZE);
                }
                else
                {
                    printf("301 Incorrect Username\n");
                    strcpy(username, "");
                    strcpy(buffer, "301");
                    send(accept_sockfd, buffer, BUFFER_SIZE, 0);
                    bzero(buffer, BUFFER_SIZE);
                }
            }
            else if (strcmp(command, "PASSWD") == 0)
            {
                command = strtok(NULL, " ");
                strcpy(password, command);
                bzero(buffer, BUFFER_SIZE);
                if (strcmp(password, PASS) == 0)
                {
                    printf("305 User Authenticated with password\n");
                    strcpy(buffer, "305");
                    send(accept_sockfd, buffer, BUFFER_SIZE, 0);
                    bzero(buffer, BUFFER_SIZE);
                }
                else
                {
                    printf("310 Incorrect password\n");
                    strcpy(password, "");
                    strcpy(buffer, "310");
                    send(accept_sockfd, buffer, BUFFER_SIZE, 0);
                    bzero(buffer, BUFFER_SIZE);
                }
                continue;
            }
            else if (strcmp(command, "CreateFile") == 0)
            {
                char *file_name;
                command = strtok(NULL, " ");
                file_name = command;
                createFile(file_name);
            }
            else if (strcmp(command, "StoreFile") == 0)
            {
                char *file_name;
                int size;
                command = strtok(NULL, " ");
                file_name = command;
                command = strtok(NULL, " ");
                size = atoi(command);
                printf("getting file : %s [%d bytes]\n", file_name, size);
                receiveFile(accept_sockfd, file_name, size);
            }
            else if (strcmp(command, "GetFile") == 0)
            {
                char *file_name;
                int size;
                command = strtok(NULL, " ");
                file_name = command;
                bzero(buffer, BUFFER_SIZE);
                if (!fileExist(file_name))
                {
                    printf("file doesnt exists\n");
                    strcpy(buffer, "400");
                    send(accept_sockfd, buffer, BUFFER_SIZE, 0);
                    bzero(buffer, BUFFER_SIZE);
                }
                else
                {
                    int size = getFileSize(file_name);
                    snprintf(buffer, BUFFER_SIZE, "%d %d", 200, size);
                    send(accept_sockfd, buffer, BUFFER_SIZE, 0);
                    bzero(buffer, BUFFER_SIZE);
                    printf("sending file : %s [%d bytes]\n", file_name, size);
                    sendFile(accept_sockfd, file_name);
                }
            }
            else if (strcmp(command, "ListDir") == 0)
            {
                bzero(buffer, BUFFER_SIZE);
                listDir(buffer);
                send(accept_sockfd, buffer, BUFFER_SIZE, 0);
                bzero(buffer, BUFFER_SIZE);
            }
            else
                printf("505 Command not supported\n");

            bzero(buffer, BUFFER_SIZE);
        }
        close(accept_sockfd);
        printf("connection closed\n");
    }
    close(sockfd);
    return (0);
}