## APPLICATION LAYOUT

1. FTAServer
    - server.c:
            the source code for the server
            - by default server listens at 127.0.0.1:4053 ( host : localhost , port :4053 )
    - Makefile :
            contains the commans to compiles and create an executable server ( ./server.o )
    - _files that should be shared to or was shared from client_

2. FTAClient
    - client.c:
            the source code for the client
            by default client sends at 127.0.0.1:4053 ( host : localhost , port :4053 )
    - Makefile :
            contains the commans to compile and create an executable client ( ./client.o )
    - _files that should be shared to or was shared from server_

## START APPLICATION

1. Start server

    1. cd into FTAServer directory
        `cd FTAServer`
    2. run make command
        `make`
    3. run the executable
        `./server.o`

        _server is started and listening to port 4053_

1. Start client

    1. cd into FTAServer directory
        `cd FTAClient`
    2. run make command
        `make`
    3. run the executable
        `./client.o`

        client is started and sending to port 4053_

    4. enter `START` command to start connection
    5. enter the username and password as given in stdout
