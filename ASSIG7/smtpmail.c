#include "header.h"

typedef struct client
{
    char username[50];
    int client_no;
    int sockfd;
    int alive;
} client;

int client_no = 0;
client *client_list[MAX_CLIENT];

int validateUser(char *username, char *password)
{
    char fusername[50], fpassword[50];
    FILE *file = fopen("userlogincred.txt", "r");
    fseek(file, 0, SEEK_SET);
    char entry[150];
    while (fgets(entry, sizeof(entry), file))
    {
        if (entry[strlen(entry) - 1] == '\n')
            entry[strlen(entry) - 1] = '\0';
        char *token = strtok(entry, " ");
        strcpy(fusername, token);
        token = strtok(NULL, " ");
        strcpy(fpassword, token);
        if (strcmp(username, fusername) == 0)
        {
            if (strcmp(password, fpassword) == 0)
            {
                printf("[+] user %s authenticated\n", username);
                return 200;
            }
            else
            {
                printf("[-] incorrect password\n");
                return 402;
            }
        }
    }

    printf("[-] invalid username\n");
    fclose(file);
    return 401;
}

client *createClient(int client_no, int sockfd, char *username)
{
    client *c = (client *)malloc(sizeof(client));
    c->client_no = client_no;
    c->sockfd = sockfd;
    c->alive = 1;
    strcpy(c->username, username);
    return c;
}

void *handleClient(void *arg)
{
    char buffer[BUFFER_SIZE];
    client *cli = (client *)arg;
    recv(cli->sockfd, buffer, BUFFER_SIZE, 0);
    if (strcmp(buffer, "EXIT") == 0)
    {
        cli->alive = 0;
        printf("[+] client %s succesfully logged out\n", cli->username);
    }
    if (strcmp(buffer, "SEND") == 0)
    {
        recv(cli->sockfd, buffer, BUFFER_SIZE, 0);
        mail *m = get_mail(buffer);

        char inbox[100], mailfile[100];
        snprintf(inbox, 100, "./mails/%s-inbox.txt", m->to);
        snprintf(mailfile, 100, "./mails/%s-mailbox.txt", m->to);

        FILE *inboxFile = fopen(inbox, "a");
        bzero(buffer, BUFFER_SIZE);
        snprintf(buffer, BUFFER_SIZE, " %s | %s | %s | %s >\n", m->from, m->to, m->timestamp, m->subject);
        fputs(buffer, inboxFile);
        fclose(inboxFile);

        FILE *mailFile = fopen(mailfile, "a");
        bzero(buffer, BUFFER_SIZE);
        snprintf(buffer, BUFFER_SIZE, "From : %s\nTo : %s\nTime : %s\nSubject : %s\nBody : %s\n", m->from, m->to, m->timestamp, m->subject, m->body);
        fputs(buffer, mailFile);
        fclose(mailFile);
    }
}

int main(int argc, char *argv[])
{
    if (argc < 1)
        error("please enter the port number");
    int sockfd, connfd, port_number;
    struct sockaddr_in server_addr, client_addr;
    char buffer1[BUFFER_SIZE], buffer2[BUFFER_SIZE];
    char username[50], password[50];
    int len = sizeof(client_addr);

    // createing a server socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
        error("socket creation failed");
    else
        printf("[+] socket created succesfully\n");
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_family = AF_INET;
    // port_number = atoi(argv[1]);
    port_number = 8080;
    server_addr.sin_port = htons(port_number);

    // binding the socket
    if ((bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr))) == -1)
        error("server binding failed");
    else
        printf("[+] server binding succesfull\n");

    // server listening
    if ((listen(sockfd, MAX_CLIENT)) == -1)
        error("server failed to listen");
    else
        printf("[+] server listening\n");

    while (1)
    {
        int connfd = accept(sockfd, (struct sockaddr *)&client_addr, &len);
        if (connfd < 0)
            error("server failed to accept the client");
        else
            printf("[+] server accepted the client\n");
        bzero(username, sizeof(username));
        bzero(password, sizeof(password));
        if (recv(connfd, &username, sizeof(username), 0) < 0)
            perror("[-] username is required\n");
        if (recv(connfd, &password, sizeof(password), 0) < 0)
            perror("[-] password is required\n");
        char response[5];
        int status = validateUser(username, password);
        sprintf(response, "%d", status);
        send(connfd, response, strlen(response), 0);
        if (status == 200)
        {
            client *c = createClient(client_no, connfd, username);
            client_list[client_no] = c;
            client_no++;
            pthread_t thread_id;
            pthread_create(&thread_id, NULL, handleClient, c);
        }
    }

    return (0);
}