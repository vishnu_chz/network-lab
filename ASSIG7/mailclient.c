#include "header.h"

// int main(int argc, char *argv[])

int main(int argc, char *argv[])
{
    if (argc < 2)
        error("please enter the port numbers");
    char *address = "127.0.0.1";
    int smtpfd, pop3fd;
    int n, len, status_code;
    int smtp_port, pop3_port;
    struct sockaddr_in smtp_addr, pop3_addr;
    char username[50], password[50], buffer[BUFFER_SIZE], message[50];

    // smtp socket
    smtpfd = socket(AF_INET, SOCK_STREAM, 0);
    smtp_port = atoi(argv[1]);
    if (smtpfd == -1)
        error("socket creation failed");
    else
        printf("[+] socket created succesfully\n");
    smtp_addr.sin_family = AF_INET;
    smtp_addr.sin_port = htons(smtp_port);

    // pop3 socket
    pop3fd = socket(AF_INET, SOCK_STREAM, 0);
    if (pop3fd == -1)
        error("pop3 socket creation failed");
    else
        printf("[+] pop3 socket created succesfully\n");
    pop3_port = atoi(argv[2]);
    pop3_addr.sin_family = AF_INET;
    pop3_addr.sin_port = htons(pop3_port);

    // validating addres for smtp
    if (inet_pton(AF_INET, address, &smtp_addr.sin_addr) <= 0)
    {
        printf("Invalid address\\Address not supported\n");
        return -1;
    }

    // validating addres for pop3
    if (inet_pton(AF_INET, address, &pop3_addr.sin_addr) <= 0)
    {
        printf("Invalid address\\Address not supported\n");
        return -1;
    }
    printf("connecting...\n");

    // connecting smtp socket
    if (connect(smtpfd, (struct sockaddr *)&smtp_addr, sizeof(smtp_addr)) < 0)
    {
        printf("SMTP Connection failed!\n");
        return -1;
    }

    printf("please enter your username and password : \n");

    printf("username: ");
    scanf("%s%*c", username);
    send(smtpfd, username, strlen(username), 0);

    printf("password: ");
    scanf("%s%*c", password);
    send(smtpfd, password, strlen(password), 0);
    recv(smtpfd, buffer, BUFFER_SIZE, 0);
    status_code = atoi(buffer);
    getMessage(message, status_code);
    if (status_code != 200)
    {
        printf("%s\n", message);
        close(smtpfd);
        exit(0);
    }

    printf("%s\n", message);
    int choice = 0;
    while (1)
    {
        printf("\nWelcome to SMTP client\n");
        printf("1. Send Mail\n");
        printf("2. Manage Mail\n");
        printf("3. Quit\n");
        printf("Select one option [1-3]: ");
        scanf("%d%*c", &choice);
        if (choice == 3)
        {
            printf("client exited\n");
            char msg[10] = "EXIT";
            send(smtpfd, msg, strlen(msg), 0);
            break;
        }

        if (choice < 1 || choice > 3)
        {
            printf("invalid choice %d\nlease try again\n", choice);
            continue;
        }

        if (choice == 1)
        {
            char msg[10] = "SEND";
            send(smtpfd, msg, strlen(msg), 0);
            printf("Compose Mail\n");
            printf("to end the body, last line should be just a period (\".\")\n");

            mail *m = (mail *)malloc(sizeof(mail));

            printf("From: %s\n", username);
            strcpy(m->from, username);
            printf("To: ");
            scanf("%s%*c", m->to);
            printf("Subject: ");
            scanf("%s%*c", m->subject);
            char *time;
            getTime(m->timestamp);
            printf("Message body:\n");
            int i = 0;
            bzero(buffer, BUFFER_SIZE);
            bzero(m->body, BODY_SIZE);
            while (strcmp(buffer, ".") != 0 && i < BUFFER_SIZE)
            {
                scanf("%[^\n]%*c", buffer);
                i += sprintf(m->body + i, "%s\n", buffer);
            }
            printf("%s\n", m->body);
            bzero(buffer, BUFFER_SIZE);
            snprintf(buffer, BUFFER_SIZE, "from:%s|to:%s|time:%s|subject:%s|body:%s\n", m->from, m->to, m->timestamp, m->subject, m->body);
            printf("%s\n", buffer);
            send(smtpfd, buffer, BUFFER_SIZE, 0);
        }
        if (choice == 2)
        {

            char ch;
            if (connect(pop3fd, (struct sockaddr *)&pop3_addr, sizeof(pop3_addr)) < 0)
            {
                printf("POP3 Connection failed!\n");
                return -1;
            }
            strcpy(buffer, username);
            send(pop3fd, buffer, BUFFER_SIZE, 0);
            printf("\nWelcome to Mail Manager\n");
            printf("[v] view inbox\n");
            printf("[a] view all inbox\n");
            printf("[d <s.no> ] delete mail\n");
            printf("[e] quit manager\n");
            while (1)
            {
                printf("Select one option [v/a/d/e] : ");
                scanf("%c", &ch);
                if (ch == 'v')
                {
                    strcpy(buffer, "VIEW");
                    send(pop3fd, buffer, BUFFER_SIZE, 0);
                    bzero(buffer, BUFFER_SIZE);
                    recv(pop3fd, buffer, BUFFER_SIZE, 0);
                    printf("\n--------------  INBOX -------------- \n\n%s\n", buffer);
                }
                if (ch == 'a')
                {
                    strcpy(buffer, "VIEWALL");
                    send(pop3fd, buffer, BUFFER_SIZE, 0);
                    bzero(buffer, BUFFER_SIZE);
                    recv(pop3fd, buffer, BUFFER_SIZE, 0);
                    printf("\n--------------  ALL MAILS -------------- \n\n%s\n", buffer);
                }
                else if (ch == 'e')
                {
                    strcpy(buffer, "EXIT");
                    send(pop3fd, buffer, BUFFER_SIZE, 0);
                    break;
                }
            }
        }
    }
    return (0);
}