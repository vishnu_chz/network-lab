#include "header.h"

typedef struct client
{
    char username[50];
    int client_no;
    int sockfd;
    int alive;
} client;

int client_no = 0;
client *client_list[MAX_CLIENT];

client *createClient(int client_no, int sockfd, char *username)
{
    client *c = (client *)malloc(sizeof(client));
    c->client_no = client_no;
    c->sockfd = sockfd;
    c->alive = 1;
    strcpy(c->username, username);
    return c;
}

void *handleClient(void *arg)
{
    char buffer[BUFFER_SIZE];
    client *cli = (client *)arg;
    printf("[+] client %s logged in \n", cli->username);
    while (1)
    {
        recv(cli->sockfd, buffer, BUFFER_SIZE, 0);
        if (strcmp(buffer, "VIEW") == 0)
        {
            char inbox[100], mailfile[100];
            snprintf(inbox, 100, "./mails/%s-inbox.txt", cli->username);
            snprintf(mailfile, 100, "./mails/%s-mailbox.txt", cli->username);
            char temp[100];
            FILE *file = fopen(inbox, "r");
            bzero(buffer, BUFFER_SIZE);
            while (fgets(temp, sizeof(temp), file))
                strcat(buffer, temp);
            printf(" buffer is :%s\n", buffer);
            printf("[+] inbox of client sent\n");
            send(cli->sockfd, buffer, BUFFER_SIZE, 0);
        }
        else if (strcmp(buffer, "VIEWALL") == 0)
        {
            char mailfile[100];
            snprintf(mailfile, 100, "./mails/%s-mailbox.txt", cli->username);
            char temp[100];
            FILE *file = fopen(mailfile, "r");
            bzero(buffer, BUFFER_SIZE);
            while (fgets(temp, sizeof(temp), file))
                strcat(buffer, temp);
            printf("[+] all mails of client sent\n");
            send(cli->sockfd, buffer, BUFFER_SIZE, 0);
        }
        else if (strcmp(buffer, "EXIT") == 0)
        {
            cli->alive = 0;
            printf("[+] client %s succesfully logged out\n", cli->username);
            bzero(buffer, BUFFER_SIZE);
        }
    }
}

int main(int argc, char *argv[])
{
    if (argc < 1)
        error("please enter the port number");
    int sockfd, connfd, port_number;
    struct sockaddr_in server_addr, client_addr;
    char buffer[BUFFER_SIZE];
    char username[50], password[50];
    int len = sizeof(client_addr);

    // createing a server socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
        error("pop3 socket creation failed");
    else
        printf("[+] pop3 socket created succesfully\n");
    bzero(&server_addr, sizeof(server_addr));
    server_addr.sin_addr.s_addr = INADDR_ANY;
    server_addr.sin_family = AF_INET;
    port_number = atoi(argv[1]);
    server_addr.sin_port = htons(port_number);

    // binding the socket
    if ((bind(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr))) == -1)
        error("pop3 server binding failed");
    else
        printf("[+] pop3 server binding succesfull\n");

    // server listening
    if ((listen(sockfd, MAX_CLIENT)) == -1)
        error("pop3 server failed to listen");
    else
        printf("[+] pop3 server listening\n");

    while (1)
    {
        int connfd = accept(sockfd, (struct sockaddr *)&client_addr, &len);
        if (connfd < 0)
            error("pop3 server failed to accept the client");
        else
            printf("[+] pop3 server accepted the client\n");
        recv(connfd, buffer, BUFFER_SIZE, 0);
        client *c = createClient(client_no, connfd, buffer);
        client_list[client_no] = c;
        client_no++;
        pthread_t thread_id;
        pthread_create(&thread_id, NULL, handleClient, c);
    }

    return (0);
}