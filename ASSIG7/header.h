#include <stdio.h>
#include <netdb.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <arpa/inet.h>
#include <time.h>
#include <pthread.h>

#define BUFFER_SIZE 2048
#define BODY_SIZE 1024
#define MAX_CLIENT 4

void error(char *msg)
{
    printf("[-] %s", msg);
    exit(0);
}

typedef struct Mail
{
    char from[50];
    char to[50];
    char timestamp[100];
    char subject[100];
    char body[BODY_SIZE];
} mail;

void getTime(char *time_string)
{
    time_t current_time;
    struct tm *timeinfo;
    current_time = time(NULL);
    timeinfo = localtime(&current_time);
    strftime(time_string, 100, "%a, %d %b %Y %I:%M:%S Localtime", timeinfo);
}

mail *get_mail(char *str)
{
    str[strlen(str) - 2] = '\0';
    mail *m = (mail *)malloc(sizeof(mail));
    char delimiter[4] = "|";
    char space[4] = ":";
    char *end_str;
    char *line = strtok_r(str, delimiter, &end_str);

    while (line != NULL)
    {
        char *end_token, *key, *value;
        char *token = strtok_r(line, space, &end_token);
        key = token;
        token = strtok_r(NULL, space, &end_token);
        value = token;
        if (strcmp(key, "from") == 0)
            strcpy(m->from, value);
        else if (strcmp(key, "to") == 0)
            strcpy(m->to, value);
        else if (strcmp(key, "time") == 0)
            strcpy(m->timestamp, value);
        else if (strcmp(key, "subject") == 0)
            strcpy(m->subject, value);
        else if (strcmp(key, "body") == 0)
            strcpy(m->body, value);
        else
            continue;
        line = strtok_r(NULL, delimiter, &end_str);
    }
    return m;
}

void show_mail(mail *m, char *string)
{
    snprintf(string, BUFFER_SIZE, "From : %s\nTo : %s\nTime : %s\nSubject : %s\nBody : %s\n", m->from, m->to, m->timestamp, m->subject, m->body);
}

void getMessage(char *msg, int status_code)
{
    switch (status_code)
    {
    case 200:
        strcpy(msg, "user succesfully authenticated");
        return;
    case 401:
        strcpy(msg, "invalid username");
        return;
    case 402:
        strcpy(msg, "incorrect password");
        return;
    case 404:
        strcpy(msg, "unable to access the client");
        return;
    default:
        return;
    }
}