#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#define LOCAL_HOST "127.0.0.1"
#define SA struct sockaddr
#define BUFFER_SIZE 255
#define PORT 8080

void get_data(char buffer[])
{
    int n = 0;
    while ((buffer[n++] = getchar()) != '\n')
        ;
    return;
}

void communicate(int sockfd)
{
    char buffer[BUFFER_SIZE];
    while (1)
    {
        bzero(buffer, sizeof(buffer));
        printf("Enter the string : ");
        get_data(buffer);

        write(sockfd, buffer, sizeof(buffer));
        if (strcmp(buffer, "Fruits\n") == 0)
        {
            memset(buffer, 0, sizeof(buffer));
            read(sockfd, buffer, sizeof(buffer));
            memset(buffer, 0, sizeof(buffer));
            get_data(buffer);
            write(sockfd, buffer, sizeof(buffer));
            memset(buffer, 0, sizeof(buffer));
            get_data(buffer);
            write(sockfd, buffer, sizeof(buffer));
        }
        memset(buffer, 0, sizeof(buffer));
        read(sockfd, buffer, sizeof(buffer));
        printf("Server Message : %s\n", buffer);

        if ((strncmp(buffer, "exit", 4)) == 0)
        {
            printf("client exited...\n");
            break;
        }
    }
}

int main()
{
    int sockfd;
    struct sockaddr_in server_addr;

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("socket successfully created...\n");
    bzero(&server_addr, sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(LOCAL_HOST);
    server_addr.sin_port = htons(PORT);

    // connect the client socket to server socket
    if (connect(sockfd, (SA *)&server_addr, sizeof(server_addr)) != 0)
    {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server...\n");

    communicate(sockfd);

    close(sockfd);
}
