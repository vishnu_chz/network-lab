#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#define LOCAL_HOST "127.0.0.1"
#define SA struct sockaddr
#define BUFFER_SIZE 255
#define PORT 8080

void get_data(char buffer[])
{
    int n = 0;
    while ((buffer[n++] = getchar()) != '\n')
        ;
    return;
}

void communicate(int sockfd, struct sockaddr_in server_addr)
{
    char buffer[BUFFER_SIZE];
    int n = 0, len;
    while (1)
    {
        bzero(buffer, sizeof(buffer));
        printf("Enter the string : ");
        get_data(buffer);

        sendto(sockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&server_addr, sizeof(server_addr));
        if (strcmp(buffer, "Fruits\n") == 0)
        {
            memset(buffer, 0, sizeof(buffer));
            n = recvfrom(sockfd, (char *)buffer, BUFFER_SIZE, MSG_WAITALL, (struct sockaddr *)&server_addr, &len);
            memset(buffer, 0, sizeof(buffer));
            get_data(buffer);
            sendto(sockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&server_addr, sizeof(server_addr));
            memset(buffer, 0, sizeof(buffer));
            get_data(buffer);
            sendto(sockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&server_addr, sizeof(server_addr));
        }
        memset(buffer, 0, sizeof(buffer));
        n = recvfrom(sockfd, (char *)buffer, BUFFER_SIZE, MSG_WAITALL, (struct sockaddr *)&server_addr, &len);
        printf("Server Message : %s\n", buffer);

        if ((strncmp(buffer, "exit", 4)) == 0)
        {
            printf("client exited...\n");
            break;
        }
    }
}

int main()
{
    int sockfd;
    struct sockaddr_in server_addr;

    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1)
    {
        error("socket creation failed...\n");
        exit(EXIT_FAILURE);
    }
    else
        printf("socket successfully created...\n");
    memset(&server_addr, 0, sizeof(server_addr));

    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(LOCAL_HOST);
    server_addr.sin_port = htons(PORT);

    communicate(sockfd, server_addr);

    close(sockfd);
    return 0;
}
