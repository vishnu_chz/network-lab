#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>
#include <arpa/inet.h>

#define SA struct sockaddr
#define BUFFER_SIZE 255
#define PORT 8080
int array_len;

typedef struct fruit
{
    char name[10];
    int count;
} fruit;

void error(char *msg)
{
    perror(msg);
    printf("\n");
    exit(0);
}

void initList(fruit *list[])
{
    int initialCount = 5;
    char *names[] = {
        "apple",
        "mango",
        "banana",
        "chikoo",
        "papaya",
    };
    int num_fruits = sizeof(names) / sizeof(names[0]);
    array_len = num_fruits;
    for (int i = 0; i < num_fruits; ++i)
    {
        fruit *newFruit = (fruit *)malloc(sizeof(fruit));
        strcpy(newFruit->name, names[i]);
        newFruit->count = initialCount;
        list[i] = newFruit;
    }
}

int pickFruit(fruit *list[], char *selected_fruit, int required_number)
{
    int flag = 0;

    fruit *target_fruit;
    selected_fruit[strlen(selected_fruit) - 1] = '\0';
    for (int i = 0; i < array_len; ++i)
    {
        if (strcmp(list[i]->name, selected_fruit) == 0)
        {
            target_fruit = list[i];
            flag = 1;
            break;
        }
    }
    if (flag == 0)
        return -1;
    else if (target_fruit->count < required_number)
        return -2;
    else
        target_fruit->count -= required_number;
    return 0;
}

void connection(int connsockfd, fruit *list[], struct sockaddr_in client_addr)
{
    char buffer[BUFFER_SIZE];
    char selected_fruit[BUFFER_SIZE];
    int required_number;
    int n = 0, len;

    // infinite loop for chat
    while (1)
    {
        bzero(buffer, BUFFER_SIZE);
        // read the message from client and copy it in buffer
        n = recvfrom(connsockfd, buffer, BUFFER_SIZE, MSG_WAITALL, (struct sockaddr *)&client_addr, &len);

        if (strcmp(buffer, "Fruits\n") == 0)
        {

            bzero(buffer, BUFFER_SIZE);
            strcpy(buffer, "Enter the fruit and count: ");
            printf("%s", buffer);
            sendto(connsockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&client_addr, len);
            bzero(buffer, BUFFER_SIZE);

            // reading the selected fruit
            n = recvfrom(connsockfd, (char *)buffer, BUFFER_SIZE, MSG_WAITALL, (struct sockaddr *)&client_addr, &len);
            strcpy(selected_fruit, buffer);
            bzero(buffer, BUFFER_SIZE);

            // reading the required number
            n = recvfrom(connsockfd, (char *)buffer, BUFFER_SIZE, MSG_WAITALL, (struct sockaddr *)&client_addr, &len);
            required_number = (atoi)(buffer);
            bzero(buffer, BUFFER_SIZE);

            int status = pickFruit(list, selected_fruit, required_number);
            switch (status)
            {
            case 0:
                strcpy(buffer, "Available");
                sendto(connsockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&client_addr, len);
                break;
            case -1:

                strcpy(buffer, "Invalid Fruit");
                sendto(connsockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&client_addr, len);
                break;
            case -2:

                strcpy(buffer, "Not Available");
                sendto(connsockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&client_addr, len);
                break;
            default:
                strcpy(buffer, "Error");
                sendto(connsockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&client_addr, len);
                break;
            }
        }
        else if (strcmp(buffer, "SendInventory\n") == 0)
        {

            char str[BUFFER_SIZE] = "\nINVENTORY\n";
            for (int i = 0; i < array_len; ++i)
            {
                char res[BUFFER_SIZE];
                snprintf(res, sizeof(res), "%s : %d\n", list[i]->name, list[i]->count);
                strcat(str, res);
            }
            printf("%s", buffer);
            strcpy(buffer, str);
            sendto(connsockfd, buffer, strlen(buffer), MSG_CONFIRM, (const struct sockaddr *)&client_addr, len);
        }

        if (strncmp("exit", buffer, 4) == 0)
        {
            printf("server exited...\n");
            break;
        }
    }
}

// Driver function
int main()
{
    int sockfd, connsockfd;
    struct sockaddr_in server_addr, client_addr;
    fruit *list[5];
    initList(list);

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd == -1)
    {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("socket created succesfully...\n");
    bzero(&server_addr, sizeof(server_addr));

    // assign IP, PORT
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(PORT);

    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA *)&server_addr, sizeof(server_addr))) != 0)
        error("socket bind failed...\n");
    else
        printf("socket successfully binded...\n");
    printf("hello");

    // Function for chatting between client and server
    connection(connsockfd, list, client_addr);

    // After chatting close the socket
    close(sockfd);
}
