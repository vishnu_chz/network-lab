#include <netdb.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <arpa/inet.h>
#define MAX 80
#define PORT 8080
#define SA struct sockaddr
void func(int sockfd)
{  
    struct sockaddr_in     servaddr;
    char buff[MAX];
    int n;
    for (;;) {
        bzero(buff, sizeof(buff));
        printf("Enter the string : ");
        n = 0;
        int len;
        while ((buff[n++] = getchar()) != '\n')
            ;
    
		sendto(sockfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr)); 
		if(strcmp(buff,"Fruits\n")==0){
		memset(buff,0,sizeof(buff)); 
		n = recvfrom(sockfd, (char *)buff, MAX,  MSG_WAITALL, (struct sockaddr *) &servaddr, &len); 
		printf("%s",buff);
		memset(buff,0,sizeof(buff)); 
		n = 0; 
		while ((buff[n++] = getchar()) != '\n') ; 
		sendto(sockfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr)); 
		memset(buff,0,sizeof(buff));      
		n = 0; 
		while ((buff[n++] = getchar()) != '\n') ; 
		sendto(sockfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &servaddr, sizeof(servaddr)); 
		}
		memset(buff,0,sizeof(buff)); 
		n = recvfrom(sockfd, (char *)buff, MAX,  MSG_WAITALL, (struct sockaddr *) &servaddr, &len); 
		printf("Message from the server : %s\n", buff);

        if ((strncmp(buff, "exit", 4)) == 0) {
            printf("Client Exit...\n");
            break;
        }
    }
}
   
int main()
{
    int sockfd, connfd;
    struct sockaddr_in servaddr, cli;
   
    // socket create and varification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));
   
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr("127.0.0.1");
    servaddr.sin_port = htons(PORT);
   
    // connect the client socket to server socket
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr)) != 0) {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");
   
    // function for chat
    func(sockfd);
   
    // close the socket
    close(sockfd);
}


