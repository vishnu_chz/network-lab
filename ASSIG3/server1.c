#include <netdb.h> 
#include <stdio.h> 
#include <stdlib.h> 
#include <string.h> 
#include <sys/socket.h> 
#include <unistd.h>
#include <arpa/inet.h>
#define MAX 80
#define MAXLINE 80
#define PORT 8080
#define SA struct sockaddr

struct fruits{
    int count;
}apple,mango,banana,chikoo,papaya;
// Function designed for chat between client and server.
void func(int connfd)
{
    struct sockaddr_in     servaddr,cliaddr;
    char buff[MAX];
    int n;
    int len=sizeof(cliaddr);;
    // infinite loop for chat
    for (;;) {
        bzero(buff, MAX);
   
        // read the message from client and copy it in buffer
       n = recvfrom(connfd, (char *)buff, MAXLINE,  MSG_WAITALL, (struct sockaddr *) &cliaddr, &len); 
        
        
        if (strcmp(buff,"Fruits\n")==0){
        bzero(buff, MAX);
        strcpy(buff,"Enter the fruit and count: ");   
        sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
        bzero(buff, MAX);
        n = recvfrom(connfd, (char *)buff, MAXLINE,  MSG_WAITALL, (struct sockaddr *) &cliaddr, &len); 
        char name[50];
        strcpy(name,buff);
        bzero(buff, MAX);
       n = recvfrom(connfd, (char *)buff, MAXLINE,  MSG_WAITALL, (struct sockaddr *) &cliaddr, &len); 
        int no=(atoi)(buff);
        bzero(buff, MAX);
        n = 0; 
         char s1[15];
         char s2[20];
        strcpy(s1,"AVAILABLE\n");
        strcpy(s2,"NOT AVAILABLE\n");
        
        if(strcmp(name,"APPLE\n")==0){
            if (apple.count>=no)   { 
              strcpy(buff,s1);   
             sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              no=apple.count-no;
              apple.count=no;}
            else{
              strcpy(buff,s2);   
              sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              }
              }
      
        else if(strcmp(name,"MANGO\n")==0){
            if (mango.count>=no)   { 
              strcpy(buff,s1);   
             sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              no=mango.count-no;
              mango.count=no;}
            else{
              strcpy(buff,s2);   
            sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              }
              }
         else if(strcmp(name,"BANANA\n")==0){
            if (banana.count>=no)   { 
              strcpy(buff,s1);   
          sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              no=banana.count-no;
              banana.count=no;}
            else{
              strcpy(buff,s2);   
             sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              }
              }
         else if(strcmp(name,"CHIKOO\n")==0){
            if (chikoo.count>=no)   { 
              strcpy(buff,s1);   
              sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              no=chikoo.count-no;
              chikoo.count=no;}
            else{
              strcpy(buff,s2);   
             sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              }
              }
         else if(strcmp(name,"PAPAYA\n")==0){
            if (papaya.count>=no)   { 
              strcpy(buff,s1);   
              sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              no=papaya.count-no;
              papaya.count=no;}
            else{
              strcpy(buff,s2);   
             sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
              }
              }
         
     }
        else if (strcmp(buff,"SendInventory\n")==0) {
      
        char string[200];
        snprintf(string,sizeof(string),"APPLE : %d ,  MANGO  : %d , BANANA : %d   , CHIKOO : %d  ,  PAPAYA  : %d  .", apple.count,mango.count,banana.count,chikoo.count,papaya.count );
        strcpy(buff,string);
             sendto(connfd, buff, strlen(buff), MSG_CONFIRM, (const struct sockaddr *) &cliaddr, len);        
        
        } 
        
       
        if (strncmp("exit", buff, 4) == 0) {
            printf("Server Exit...\n");
            break;
        }
    }
}
   
// Driver function
int main()
{
    int sockfd, connfd, len;
    struct sockaddr_in servaddr, cli;
        apple.count=10;

	mango.count=10;
	banana.count=10;
	chikoo.count=10;
	papaya.count=10;
    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));
   
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);
   
    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
        printf("socket bind failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully binded..\n");
   
    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0) {
        printf("Listen failed...\n");
        exit(0);
    }
    else
        printf("Server listening..\n");
    len = sizeof(cli);
   
    // Accept the data packet from client and verification
    connfd = accept(sockfd, (SA*)&cli, &len);
    if (connfd < 0) {
        printf("server accept failed...\n");
        exit(0);
    }
    else
        printf("server accept the client...\n");
   
    // Function for chatting between client and server
    func(connfd);
   
    // After chatting close the socket
    close(sockfd);
}






