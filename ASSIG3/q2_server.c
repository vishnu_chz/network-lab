
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/fcntl.h>
#include <unistd.h>
#include <netdb.h>
#include<signal.h>

#define PORT 8080
#define SA struct sockaddr

int main(){
    int sockfd, connfd, len, newsockfd,n1,n2,flag;
    struct sockaddr_in servaddr, cli;
    pid_t childpd;
    char buffer[1024];

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    bzero(&servaddr, sizeof(servaddr));
   
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
    servaddr.sin_port = htons(PORT);
   
    // Binding newly created socket to given IP and verification
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
        printf("socket bind failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully binded..\n");
   
    // Now server is ready to listen and verification
    if ((listen(sockfd, 5)) != 0) {
        printf("Listen failed...\n");
        exit(0);
    }
    else
        printf("Server listening..\n");
    //len = sizeof(cli);
   
    /*
    // Accept the data packet from client and verification
    connfd = accept(sockfd, (SA*)&cli, &len);
    if (connfd < 0) {
        printf("server accept failed...\n");
        exit(0);
    }
    else
        printf("server accept the client...\n");
    */
    while(1){
        len = sizeof(servaddr);
        newsockfd = accept(sockfd,(SA*)&cli, (socklen_t *)&len);
        if(newsockfd < 0){
            perror("Acceptance error");
            exit(1);
        }
        else{ 
            printf("Connected to %s  : %d\n",inet_ntoa(cli.sin_addr),ntohs(cli.sin_port));


        }
        if ((childpd = fork())==0){
            close(sockfd);
            while(1){
                bzero(buffer,1024);
                recv(newsockfd,buffer,sizeof(buffer),0);
                
                if(buffer[0] =='\0'){
                    printf("Client %d Disconnected\n",ntohs(cli.sin_port));
                    exit(0);
                }
                else{
                    printf("Clinet %d message : %s\n", ntohs(cli.sin_port),buffer);
                    flag=0;
                    n1=0;
                    n2=0;
                    int op;

                    for(int i=0;i<strlen(buffer)-1;i++){
                        if (buffer[i] == '+' || buffer[i] == '-' || buffer[i] == '*' || buffer[i] == '/' ){
                            flag=1;
                            op=i;
                        }
                        else{
                            if (flag==0){
                                n1=(n1*10)+(buffer[i]-'0');
                            }
                            else if (flag != 0){
                                n2=(n2*10)+(buffer[i]-'0');
                            }
                        }
                    }
                    int result=0;
                    //calculate
                    if(buffer[op]== '+')
                        result = n1+n2;
                    else if(buffer[op]== '-')
                        result = n1-n2;
                    else if(buffer[op]== '/')
                        result = n1/n2;
                    else if(buffer[op]== '*')
                        result = n1*n2;
                    
                    printf("Sending Reply to client %d : Answer :%i\n", ntohs(cli.sin_port), result);

                    bzero(buffer,1024);
                    sprintf(buffer, "%d", result);
                    send(newsockfd, buffer, strlen(buffer),0 );
                    bzero(buffer,1024);
                    
                } 

            }
        }


    }
    if (read(newsockfd , buffer, 1024) <= 0)
        printf("CLosing Server...\n");
    
    close(newsockfd);
    return 0;

    

}