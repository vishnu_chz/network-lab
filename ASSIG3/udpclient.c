#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <netinet/in.h>
   
#define PORT     8080
#define MAX 1024

int main(){

    char buff[MAX];
    char * hello="hello from GG";
    char *s;
    
    struct sockaddr_in servaddr, cliaddr;

    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);
    if (sockfd<0){
        perror("socket creation failed");
        exit(EXIT_FAILURE);
        
    }
    memset(&servaddr, 0, sizeof(servaddr));

    // Filling server information
    servaddr.sin_family = AF_INET;
    servaddr.sin_port = htons(PORT);
    servaddr.sin_addr.s_addr = INADDR_ANY;

    int n, len;
    /*   
    sendto(sockfd, (const char *)hello, strlen(hello),
        MSG_CONFIRM, (const struct sockaddr *) &servaddr, 
            sizeof(servaddr));
    printf("Hello message sent.\n");
           
    n = recvfrom(sockfd, (char *)buff, MAX, 
                MSG_WAITALL, (struct sockaddr *) &servaddr,
                &len);
    buff[n] = '\0';
    printf("Server : %s\n", buff);
    */

    for(;;){
        bzero(buff, sizeof(buff));
		printf("\nEnter the string : ");
		n = 0;
		while ((buff[n++] = getchar()) != '\n')
			;
        
        //write(sockfd, buff, sizeof(buff));
        sendto(sockfd, buff, strlen(buff),
        MSG_CONFIRM, (const struct sockaddr *) &servaddr, 
            sizeof(servaddr));
		if ((strncmp(buff, "exit", 4)) == 0) {
			printf("Client Exit...\n");
			break;
		}
		bzero(buff, sizeof(buff));
		//read(sockfd, buff, sizeof(buff));
        n = recvfrom(sockfd, buff, MAX, 
                MSG_WAITALL, (struct sockaddr *) &servaddr,
                &len);

		printf("From Server : %s", buff);
		if ((strncmp(buff, "exit", 4)) == 0) {
			printf("Client Exit...\n");
			break;
		}
    }



   
    close(sockfd);
    return 0;

}